Welcome to the AIMS Desktop documentation
=========================================

.. toctree::
   :maxdepth: -1
   :caption: Contents

   getting-started/installation
   howto/howto

